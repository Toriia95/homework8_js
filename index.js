const PARAGRAPH_TAG = 'p';
const RED_COLOR = '#ff0000';

const paragraphs = document.getElementsByTagName(PARAGRAPH_TAG);

for (let i = 0; i < paragraphs.length; i++) {
    paragraphs[i].style.backgroundColor = RED_COLOR;
}

const optionList = document.getElementById('optionsList');
console.log(optionList);

const parent = optionList.parentElement;
console.log(parent);

const optionListChildren = optionList.children;

if (optionListChildren?.length) {
    for (let i = 0; i < optionListChildren.length; i++) {
        console.log(optionListChildren[i].nodeName);
        console.log(optionListChildren[i].nodeType);
    }
}

const testParagraph = document.getElementById('testParagraph');
testParagraph.innerHTML = "This is a paragraph";

const mainHeaderChildren = document.getElementsByClassName('main-header')?.[0]?.children;

for (let i = 0; i < mainHeaderChildren.length; i++) {
    console.log(mainHeaderChildren[i]);
    mainHeaderChildren[i].classList.add('nav-list');
}

const sectionTitleElements = document.getElementsByClassName('section-title');

while(sectionTitleElements.length) {
    sectionTitleElements[0].classList.remove('section-title');
}
